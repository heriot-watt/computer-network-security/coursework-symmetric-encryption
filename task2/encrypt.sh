#!/bin/bash

#--------------- Formatting Output ----------------

output=""

function dim() { echo -e "\e[2m$1\e[0m"; }
function bold() { echo -e "\e[1m$1\e[0m"; }
function green() { echo -e "\e[92m$1\e[39m"; }
function blue() { echo -e "\e[94m$1\e[39m"; }
function red() { echo -e "\e[91m$1\e[39m"; }
function format_encryption_details() {
    cipher=$1
    mode=$2
    
    output+="\n$(bold "Cipher:")|$(blue ${cipher['name']^^})|$(bold "Mode:") $(blue ${mode^^})\
            \n$(bold "Key:")|$(dim "(${cipher['key']} bits)")|$(red $4)\
            \n$(bold "IV:")|$(dim "(${cipher['iv']} bits)")|$(red $5)\
            \n$(bold "Ouput:")|$(green "./$3.bin")\n"
}

#------------- Encryption with ciphers ------------

declare -a modes=('cbc' 'cfb' 'ofb')
declare -A cipher0=(['name']='aes-128'  ['key']=128  ['iv']=128)
declare -A cipher1=(['name']='des'      ['key']=64   ['iv']=64)
declare -A cipher2=(['name']='bf'       ['key']=128  ['iv']=64)
declare -n cipher

function hex_gen() { echo $(openssl rand -hex $(($1/8))); }

for cipher in ${!cipher@}; do
    for mode in ${modes[@]}; do
        key=$(hex_gen ${cipher['key']})
        iv=$(hex_gen ${cipher['iv']})
        name=${cipher['name']}-$mode

        openssl enc -$name -e -in plain.txt -out ${name}.bin -K $key -iv $iv

        format_encryption_details "$cipher" $mode $name $key $iv
    done
done

#----------------- Logging Output -----------------

echo -e $output | column -s"|" -t -e
