#!/bin/bash

function extract_bytes() {
    echo $(echo "$1" | grep -oP "[0123456789abcdef]{2}")
}

function xor() {
    result=""
    bytes1=($(extract_bytes $1))
    bytes2=($(extract_bytes $2))

    (( ${#bytes1[@]} != ${#bytes2[@]} )) && echo "Error: cannot run xor on 2 hex strings of different sizes"

    for i in ${!bytes1[@]}; do
        hex_byte1="0x${bytes1[i]}"
        hex_byte2="0x${bytes2[i]}"
        
        result="$result$(printf "%02x" "$(($hex_byte1 ^ $hex_byte2))")"
    done

    echo "$result"
}

for i in "$@"; do
    case $i in
        -c1=* | --ciphertext1=*)
            c1=$(cat "${i#*=}")
            shift
            ;;
        -p1=* | --plaintext1=*)
            p1=$(cat "${i#*=}" | xxd -p)
            shift
            ;;
        -c2=* | --ciphertext2=*)
            c2=$(cat "${i#*=}")
            shift
            ;;
    esac
done

[[ -z $c1 ]] && echo "Error: missing argument --ciphertext1" && exit 1
[[ -z $p1 ]] && echo "Error: missing argument --plaintext1" && exit 1
[[ -z $c2 ]] && echo "Error: missing argument --ciphertext2" && exit 1

echo -e "\nDecrypting message..."

result=$(xor $c1 $p1)
result=$(xor $result $c2)

echo -e "Decrypted: '$(echo "$result" | xxd -r -p)'"
