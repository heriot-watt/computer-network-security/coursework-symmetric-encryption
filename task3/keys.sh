#!/bin/bash

# Cipher:  AES-128            Mode: ECB 
# Key:     (128 bits)         afed733d8e0b20c1094c08d652eae122 
# IV:      (- bits)            
# Ouput:   ./aes-128-ecb.bin

keyspath='./keys.txt'
save=0
load=0
cipher=""
mode=""
file=""
key=""
iv=""

function dim() { echo -e "\e[2m$1\e[0m"; }
function bold() { echo -e "\e[1m$1\e[0m"; }
function green() { echo -e "\e[92m$1\e[39m"; }
function blue() { echo -e "\e[94m$1\e[39m"; }
function red() { echo -e "\e[91m$1\e[39m"; }
function print_format() {
    keylength=$((${#key} * 4))
    [[ -z $iv ]] && ivlength='-' || ivlength=$((${#iv} * 4))
    
    output="\n$(bold "Cipher:")|$(blue ${cipher^^})|$(bold "Mode:") $(blue ${mode^^})\
            \n$(bold "Key:")|$(dim "($keylength bits)")|$(red $key)\
            \n$(bold "IV:")|$(dim "($ivlength bits)")|$(red $iv)\
            \n$(bold "Ouput:")|$(green "./$file")"

    echo -e $output | column -s"|" -t -e
}

function format() {
    keylength=$((${#key} * 4))
    [[ -z $iv ]] && ivlength='-' || ivlength=$((${#iv} * 4))

    output="\nCipher:|${cipher^^}|Mode: ${mode^^}\
            \nKey:|($keylength bits)|$key\
            \nIV:|($ivlength bits)|$iv\
            \nOuput:|./$file\n"

    echo -e $output | column -s"|" -t -e >> $keyspath
}

function extract_length() {
    echo "$(grep -oP "\(\K\d+" <<< $1)"
}

function extract_hex() {
    echo "$(grep -oP "[0123456789abcdef]{$(($2 / 4))}" <<< $1)"
}

for i in "$@"
do
    case $i in
        -S | --save)
            save=1
            shift
            ;;
        -L | --load)
            load=1
            shift
            ;;
        -c=* | --cipher=*)
            cipher="${i#*=}"
            shift
            ;;
        -m=* | --mode=*)
            mode="${i#*=}"
            shift
            ;;
        -f=* | --file=*)
            file="${i#*=}"
            shift
            ;;
        -K=* | --key=*)
            key="${i#*=}"
            shift
            ;;
        -iv=*)
            iv="${i#*=}"
            shift
            ;;
    esac
done

if (( $save ^ $load == 0 )); then
    echo "Error: invalid arguments, choose between either saving or loading"
    exit 1
elif (( $save == 1 )); then
    [[ -z $cipher ]] && echo "Error: missing argument --cipher" && exit 1
    [[ -z $mode ]] && echo "Error: missing argument --mode" && exit 1
    [[ -z $file ]] && echo "Error: missing argument --file" && exit 1
    [[ -z $key ]] && echo "Error: missing argument --key" && exit 1

    format $cipher $mode $name $key $iv
    print_format $cipher $mode $name $key $iv

elif (( $load == 1 )); then
    [[ -z $file ]] && echo "Error: missing argument --file" && exit 1
    [[ ! -f $keyspath ]] && echo "Error: enable to find $keyspath" && exit 1

    declare -a arr=()

    while IFS= read -r line
    do
        [[ -z $line ]] && arr=() || arr+=("$line")
        [[ $line == *"$file"* ]] && break
    done < "$keyspath"

    if (( ${#arr} > 0 )); then
        for line in "${arr[@]}"; do
            if [[ $line = Key:* ]]; then
                keylength=$(extract_length "$line")
                echo "Key: $(extract_hex "$line" $keylength)"
            elif [[ $line = IV:* ]]; then
                ivlength=$(extract_length "$line")
                [[ ! -z $ivlength ]] && echo "IV:  $(extract_hex "$line" $ivlength)"
            fi
        done
    else
        echo "Error: couldn't find encryption details"
    fi
fi
