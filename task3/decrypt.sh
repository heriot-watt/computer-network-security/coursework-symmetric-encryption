#!/bin/bash

corrupted="--corrupted"

[[ "$1" == "$corrupted" ]] && arr=$(find . -type f \( -iname "*.corrupted.bin" \)) \
                             || arr=$(find . -type f \( -iname "*.bin" ! -iname "*.corrupted.bin" \))

function extract_hex() {
    echo "$(grep -oP "$2\s+\K[0123456789abcdef]+" <<< $1)"
}

for elt in ${arr[@]}; do
    base=$(basename "$elt" .bin)
    name=$(basename "$base" .corrupted)
    echo -e "Decrypting of cipher $name..."

    key=""
    iv=""

    keyiv="$(./keys.sh "-L" "--file=$name.bin")"

    while IFS= read -r line
    do
        [[ $line == "Key:"* ]] && key=$(extract_hex "$line" "Key:")
        [[ $line == "IV:"* ]] && iv=$(extract_hex "$line" "IV:")
    done <<< $keyiv

    if [[ -z $iv ]]; then
        openssl enc "-$name" -d -in "${base}.bin" -out "${base}.txt" -K "$key"
    else
        openssl enc "-$name" -d -in "${base}.bin" -out "${base}.txt" -K "$key" -iv "$iv"
    fi
done
