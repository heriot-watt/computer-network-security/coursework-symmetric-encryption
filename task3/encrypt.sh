#!/bin/bash

declare -A mode0=(['mode']='ecb' ['iv']=0)
declare -A mode1=(['mode']='cbc' ['iv']=1)
declare -A mode2=(['mode']='cfb' ['iv']=1)
declare -A mode3=(['mode']='ofb' ['iv']=1)
declare -n mode
declare -A cipher=(['name']='aes-128'  ['key']=128  ['iv']=128)

function hex_gen() { echo $(openssl rand -hex $(($1/8))); }

[[ -f "keys.txt" ]] && rm "keys.txt"

for mode in ${!mode@}; do
    name=${cipher['name']}-${mode['mode']}
    echo -e "\nEncrypting with cipher $name..."

    key=$(hex_gen ${cipher['key']})

    if [ "${mode['iv']}" -eq 1 ]; then
        iv=$(hex_gen ${cipher['iv']})
        openssl enc -$name -e -in plain.txt -out ${name}.bin -K $key -iv $iv
    else
        openssl enc -$name -e -in plain.txt -out ${name}.bin -K $key
    fi

    ./keys.sh "-S" "--cipher=${cipher['name']}" "-m=${mode['mode']}" "--file=$name.bin" "-K=$key" "-iv=$iv"
done
