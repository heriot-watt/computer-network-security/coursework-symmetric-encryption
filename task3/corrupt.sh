#!/bin/bash

pos=54

function corrupt() {
    if (( $(stat -c%s "$1") > $pos+1 )); then
        cp $1 $2
        hex="0x$(xxd -p -l1 -s $pos $1)"
        printf "%02x" "$(($hex ^ 1))" | xxd -p -r | dd of=$2 bs=1 seek=$pos count=1 conv=notrunc
    else
        echo -e "Error: binary file have less than $(($pos + 1)) bytes"
        exit 1
    fi
}

for file in $(find . -type f \( -iname "*.bin" ! -iname "*.corrupted.bin" \)); do
    input="$(basename -- "$file")"
    output="$(basename -- "$file" .bin).corrupted.bin"
    echo "Corrupting $input into $output..."
    corrupt $input $output &> /dev/null
done
