from itertools import product
import sys

from pprint import pprint

from constants import ALPHABET
from utils import check_if_txt

from pattern import Node, NodeList


# Count the number of letters in a given text
def count(text: str):
    count = 0

    for letter in ALPHABET:
        count += text.count(letter)

    return count

# Returns a dictionary {'pattern': coeff} representing the frequency for each pattern
# Argument 'n' is for the length of the pattern
def freq(data: str, n: int, details = False):
    level = 1 if (n < 1) else n
    result = NodeList()

    patterns = [''.join(elt) for elt in list(product(ALPHABET, repeat=level))]

    text = data.replace(' ', '')
    text = text.replace('\n', '')
    text = text.lower()

    nb_letters = count(text)

    for pattern in patterns:
        f = text.count(pattern)
        coeff = f/nb_letters*100

        if (coeff != 0):
            result.add(Node(pattern, f, coeff) if (details) else Node(pattern, coeff))

    return result


if __name__ == '__main__':
    if (len(sys.argv) > 1):
        check_if_txt(sys.argv[1])
        iterations = 1

        if ((len(sys.argv) > 2) and sys.argv[2].isdigit()):
            iterations = int(sys.argv[2])

        with open(sys.argv[1], 'r') as file:
            text = file.read()
            print(freq(text, iterations, details=True).sorted().reverse())
    else:
        raise Exception("Error: missing argument, please give a file with the extension '.txt'")
