from typing import Dict

from constants import ALPHABET

class Key:

    def __init__(self):
        self._mapping = {key:'?' for key in ALPHABET}

    @staticmethod
    def from_str(key: str) -> 'Key':
        Key._validate_key(key)
        inst = Key()

        for i in range(0, len(ALPHABET)):
            inst[ALPHABET[i]] = key[i]

        return inst

    @staticmethod
    def load(path) -> 'Key':
        with open(path, 'r') as file:
            data = file.readlines()

            if (len(data) == 2):
                Key._validate_key(data[1])
                return Key.from_str(data[1])
            else:
                raise Exception("Error: file content invalid for laoding key")

    def save(self, path):
        with open(path, 'w+') as file:
            file.write(''.join(ALPHABET).upper() + '\n' + str(self))
    
    @staticmethod
    def _validate_key(key: str):
        key_arr = [elt for elt in key]
        key_arr.sort()

        if (len(key_arr) != 26 or key_arr != ALPHABET):
            raise Exception("Error: the key format is invalid")

    @staticmethod
    def _validate_item(key):
        if (key not in ALPHABET):
            raise Exception("Error: given letter is invalid")

    def value_in_use(self, value) -> bool:
        return value in self._mapping.values()

    def __getitem__(self, letter) -> str:
        Key._validate_item(letter)
        return self._mapping[letter]

    def __setitem__(self, letter, value):
        Key._validate_item(letter)
        Key._validate_item(value)

        if (self._mapping[letter] != value and self.value_in_use(value)):
            raise Exception("Error: value already used for another letter")

        self._mapping[letter] = value

    def evaluate(self, valid_key: 'Key') -> Dict[str, bool]:
        return {letter.upper():(value == valid_key[letter]) for (letter, value) in self._mapping.items()}

    def __str__(self) -> str:
        return ''.join([value for (key, value) in self._mapping.items()])
