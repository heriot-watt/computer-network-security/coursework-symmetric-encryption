import sys
from typing import List
from pprint import pprint

from utils import check_if_txt
from constants import ALPHABET, LETTERS_SEQ_FREQ_ARR
import frequency

from key import Key
from pattern import Node, NodeList, Pattern, PatternDict, Mapping


diff = lambda x,y: abs(x - y)


def evaluate(freq: NodeList, patterns_freq: NodeList):
    result = PatternDict()
    priority = 1

    freq_o = freq.sorted().reverse()
    patterns_freq_o = patterns_freq.sorted().reverse()

    for i, node in enumerate(patterns_freq_o):
        arr = [Node(elt.pattern, (diff(node.coeff, elt.coeff)**(0.5))*(abs(i-j)+2 if (j >= i) else abs(i-j)+1)) for (j, elt) in enumerate(freq_o)]
        result[node.pattern] = Pattern(priority, NodeList(arr).sorted())
        priority += 1

    return result

# Attack on frequency for each patterns of letters
def attack_n(text: str, n: int):
    if (n > 0 and n <= len(LETTERS_SEQ_FREQ_ARR)):
        return evaluate(frequency.freq(text, n), LETTERS_SEQ_FREQ_ARR[n-1])
    else:
        raise Exception("Error: index out of bound")

def approximate(data: PatternDict):
    result = PatternDict()

    for (key, pattern) in data.sorted(byPriority=True):
        for node in pattern.list:
            for i in range(0, len(key)):
                if len(list(filter(lambda x: x == key[i], result.keys()))) == 0:
                    result[key[i]] = Pattern(pattern.priority)

                if len(list(filter(lambda x: x.pattern == node.pattern[i], result[key[i]].list))) == 0:
                    result[key[i]].add(Node(node.pattern[i], node.coeff))

    return result

def combine(prev_data: PatternDict, data: PatternDict):
    result = PatternDict()

    for letter in ALPHABET:
        if (letter in prev_data.keys()):
            for i, prev_node in enumerate(prev_data[letter].list):
                if (letter in data.keys()):
                    for j, node in enumerate(data[letter].list):
                        if (prev_node.pattern == node.pattern):
                            if (letter not in result.keys()):
                                result[letter] = Pattern(diff(prev_data[letter].priority, data[letter].priority))

                            coeff = result[letter].priority**2 * (abs(i-j)+2 if (j >= i) else abs(i-j)+1)
                            result[letter].add(Node(prev_node.pattern, coeff * diff(prev_node.coeff, node.coeff)))
                else:
                    result[letter] = Pattern(prev_data[letter].priority, prev_data[letter].list)

        if (letter in result.keys()):
            result[letter] = Pattern(result[letter].priority, result[letter].list.sorted())

    return result


def guess_key(data: PatternDict):
    mapping = Mapping.from_pattern_dict(data)
    key = Key()

    for letter in data.keys(sorted=True):
        for elt in mapping[letter]:
            if (not key.value_in_use(elt)):
                key[letter] = elt
                break

    return key

# Main attack function
def attack(ciphertext_path: str):
    check_if_txt(ciphertext_path)

    text = ""
    with open(ciphertext_path, 'r') as file:
        text = file.read()
    
    result = None

    for i in range(1, 4):#len(LETTERS_SEQ_FREQ_ARR)+1):
        print("Attack on " + str(i) + " letter(s) sequence...")
        if result is None:
            result = approximate(attack_n(text, i))
        else:
            result = combine(result, approximate(attack_n(text, i)))

    return guess_key(result)

# Compare result of attack with actual ciphertext key
def test(ciphertext_path: str, key_path: str):
    check_if_txt(key_path, 'key')

    valid_key = Key.load(key_path)
    key = attack(ciphertext_path)

    print("\nAlphabet:   " + ''.join(ALPHABET).upper())
    print("Cipher key: " + str(valid_key))
    print("Prediction: " + str(key))
    print("Precision: " + str(round(len(list(filter(lambda x: x[1], key.evaluate(valid_key).items())))/len(ALPHABET)*100, 2)) + "%")
    pprint(key.evaluate(valid_key))


if __name__ == '__main__':
    if (len(sys.argv) == 3):
        test(sys.argv[1], sys.argv[2])
    else:
        raise Exception("Error: missing argument, please give a file with the extension '.txt'")
