from constants import ALPHABET

# Create a dictionary of words
def create_dict():
    result = []

    with open('/usr/share/dict/words', 'r') as file:
        data = file.read()
        dictionary = data.splitlines()

        for elt in dictionary:
            word = elt.lower()
            
            if (len([elt for elt in word if elt not in ALPHABET]) == 0):
                result.append(word)

    return result


if __name__ == '__main__':
    print("\nCreating dictionary...")
    result = create_dict()
    print("Created dictionary of {} words.".format(len(result)))
