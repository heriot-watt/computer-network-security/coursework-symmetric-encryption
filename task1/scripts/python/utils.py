import sys
import os

# Verify that the given file exists and with the extension '.txt'
def check_if_txt(path: str, label: str = "text"):
    if (not(os.path.isfile(path)) or path[-4:] != ".txt"):
        raise Exception("Error: the " + label + " file needs to exist with the extension '.txt'")


if __name__ == '__main__':
    if (len(sys.argv) > 1):
        check_if_txt(sys.argv[1])
    else:
        raise Exception("Error: missing argument, please give a file with the extension '.txt'")
