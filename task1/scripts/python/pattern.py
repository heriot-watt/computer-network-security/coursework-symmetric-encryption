from typing import List, Tuple, Iterator


class Node:

    def __init__(self, pattern: str, coeff: float, count: int = -1):
        self._pattern = pattern
        self._coeff = coeff
        self._count = count

    @property
    def pattern(self) -> str:
        return self._pattern

    @property
    def coeff(self) -> float:
        return self._coeff

    def __str__(self) -> str:
        if (self._count != -1):
            return "(%s, (%d, %.2f))" % (self._pattern, self._coeff, self._count)
        else:
            return "(%s, %.2f)" % (self._pattern, self._coeff)


class NodeList:

    def __init__(self, arr: List[Node] = []):
        self._list = [node for node in arr]

    @staticmethod
    def from_tuple_list(tuple_arr: List[Tuple[str, float]]) -> 'NodeList':
        node_list = NodeList()

        for (pattern, coeff) in tuple_arr:
            node_list.add(Node(pattern, coeff))

        return node_list

    def __len__(self) -> int:
        return len(self._list)

    def __iter__(self) -> Iterator[Node]:
        return iter(self._list)

    @property
    def pattern_length(self):
        if (len(self) > 0):
            return len(self._list[0].pattern)
        else:
            return -1

    def add(self, node: Node) -> int:
        if (node.pattern in [node.pattern for node in self._list]):
            raise Exception("Error: node with pattern '%s' already exists in list" % node.pattern)
        elif (self.pattern_length != -1 and len(node.pattern) != self.pattern_length):
            raise Exception("Error: pattern length is invalid")
        else:
            return self._list.append(node)

    def sorted(self) -> 'NodeList':
        return NodeList(sorted(self._list, key=lambda elt: elt.coeff))

    def reverse(self) -> 'NodeList':
        return NodeList(self._list[::-1])

    def strip(self) -> List[str]:
        return [node.pattern for node in self]

    def __str__(self) -> str:
        return '[' + ",\n ".join([str(node) for node in self._list]) + ']'


class Pattern:

    def __init__(self, priority: int = -1, arr: NodeList = None):
        self._list = arr if (arr is not None) else NodeList()
        self.priority = priority

    @property
    def pattern_length(self):
        return self._list.pattern_length

    @property
    def list(self) -> NodeList:
        return self._list

    def add(self, node: Node) -> int:
        return self._list.add(node)

    def strip(self):
        return self._list.strip()

    def __str__(self) -> str:
        return '('  + str(self.priority) + ",\n " + str(self._list).replace("\n ", "\n  ") + ')'

class PatternDict:

    def __init__(self):
        self._dict = {}

    def __getitem__(self, key) -> Pattern:
        if (key not in self._dict.keys()):
            raise Exception("Error: given key is invalid")
        else:
            return self._dict[key]

    def __setitem__(self, key, pattern: Pattern):
        if (self.pattern_length != -1 and pattern.pattern_length != -1):
            if ((pattern.pattern_length != self.pattern_length) or (len(key) != pattern.pattern_length)):
                raise Exception("Error: key or pattern length is invalid")
        
        self._dict[key] = pattern

    def __iter__(self) -> Iterator[Tuple[str, Pattern]]:
        return iter(self._dict.items())

    def __len__(self):
        return len(self._dict.items())

    @property
    def pattern_length(self):
        if (len(self) > 0):
            return len(list(self._dict.keys())[0])
        else:
            return -1

    def sorted(self, byPriority: bool = False) -> Iterator[Tuple[str, Pattern]]:
        keyByAlphabetic = lambda elt: elt[0]
        keyByPriority = lambda elt: elt[1].priority
        return sorted(self, key=keyByPriority if (byPriority) else keyByAlphabetic)

    def keys(self, sorted: bool = False) -> List[str]:
        return [key for (key, pattern) in (self.sorted(byPriority=True) if (sorted) else self)]

    def __str__(self) -> str:
        return '{' + ",\n ".join([key + ': ' + str(pattern).replace("\n ", "\n     ") for (key, pattern) in self.sorted()]) + '}'

class Mapping:

    def __init__(self):
        self._dict = {}

    @staticmethod
    def from_pattern_dict(pattern_dict: PatternDict) -> 'Mapping':
        if (pattern_dict.pattern_length != 1):
            raise Exception("Error: pattern dictionary needs to have a pattern lenght of 1")

        mapping = Mapping()

        for (key, pattern) in pattern_dict:
            mapping[key] = pattern.list.strip()

        return mapping

    def __getitem__(self, key):
        if (key not in self._dict.keys()):
            raise Exception("Error: given key is invalid")
        else:
            return self._dict[key]

    def __setitem__(self, key, arr: List[str]):
        arr2 = []

        for elt in arr:
            if (len(elt) != 1 or elt in arr2):
                raise Exception("Error: given array of letter is invalid")
            
            arr2.append(elt)
        
        self._dict[key] = arr2

    def __str__(self):
        return '{' + ",\n ".join([key + ': ' + str(arr) for (key, arr) in self._dict.items()]) + '}'
