import random
import sys
import os

from constants import ALPHABET, PUNCTUATION, WORD_SEPARATIONS
from utils import check_if_txt

# Monoalphabetic cipher handling encryption and decryption 
class Cipher:
    usage = "\nUsage description:"\
        + "\n  - $ python3 cipher.py help"\
        + "\n  - $ python3 cipher.py encrypt plaintext.txt"\
        + "\n  - $ python3 cipher.py decrypt ciphertext.txt key.txt"

    # Override utils function to add context
    @staticmethod
    def __check_if_txt(path: str, label: str):
        try:
            check_if_txt(path, label)
        except Exception as e:
            raise Exception(str(e) + "\n" + Cipher.usage)

    # Create a random key for the ciphertext
    @staticmethod
    def __create_key():
        alphabet = [elt for elt in ALPHABET]
        key = []

        while (len(alphabet) != 0):
            index = random.randint(0, 1000)%len(alphabet)
            key.append(alphabet[index])
            alphabet.remove(alphabet[index])

        return key

    # Verify a given ciphertext key
    @staticmethod
    def __verify_key(key: str):
        key_arr = [elt for elt in key]
        key_arr.sort()

        if (len(key_arr) != 26 or key_arr != ALPHABET):
            raise Exception("Error: the key format is invalid")

    # Format plaintext by putting every letter lowercase and removing punctuations
    @staticmethod
    def __format_plaintext(plaintext: str, key: str):
        text = plaintext.lower()

        for elt in PUNCTUATION:
            text = text.replace(elt, '')

        text = text.replace(' - ', ' ')
        text = text.replace('-', ' ')

        arr = [elt for elt in text if elt not in ALPHABET and elt not in WORD_SEPARATIONS]
        if (len(arr) != 0):
            raise Exception("Error: the plaintext should be formated for it to contain no special characters\n" + str(arr) + "\n" + Cipher.usage)
        else:
            return text

    # Encryption method: create a key, format plaintext, encrypt, then save ciphertext & key
    @staticmethod
    def encrypt(plaintext_path: str):
        path = os.path.dirname(os.path.abspath(plaintext_path))
        text = ""

        Cipher.__check_if_txt(plaintext_path, "plaintext")

        key = Cipher.__create_key()
        
        with open(os.path.join(path, 'key.txt'), 'w+') as file:
            file.write(''.join(ALPHABET).upper()+'\n')
            file.write(''.join(key))

        with open(plaintext_path, 'r') as file:
            text = file.read()
        
        text = Cipher.__format_plaintext(text, key)
        
        for i in range(0, len(ALPHABET)):
            text = text.replace(ALPHABET[i], key[i].upper())

        with open(os.path.join(path, 'ciphertext.txt'), 'w+') as file:
            file.write(text.lower())

    # Decryption: verify the given key, decrypt, then print the plaintext
    @staticmethod
    def decrypt(ciphertext_path: str, key_path: str):
        key = ""
        text = ""

        Cipher.__check_if_txt(ciphertext_path, "ciphertext")
        Cipher.__check_if_txt(key_path, "key")

        with open(key_path, 'r') as file:
            key = file.readlines()
            Cipher.__verify_key(key[1])

        with open(ciphertext_path, 'r') as file:
            text = file.read()
        
        for i in range(0, len(ALPHABET)):
            text = text.replace(key[1][i], ALPHABET[i].upper())

        print(text)


if __name__ == '__main__':
    error = Exception("Error: invalid program arguments\n" + Cipher.usage)

    try:
        if (len(sys.argv) < 2):
            raise error
        elif (sys.argv[1] == "encrypt"):
            if (len(sys.argv) != 3):
                raise error
            else:
                Cipher.encrypt(sys.argv[2])
        elif (sys.argv[1] == "decrypt"):
            if (len(sys.argv) != 4):
                raise error
            else:
                Cipher.decrypt(sys.argv[2], sys.argv[3])
        elif (sys.argv[1] == "help" or sys.argv[1] == "-h" or sys.argv[1] == "--help"):
            print(Cipher.usage)
        else:
            raise error
    except Exception as e:
        print(e)
