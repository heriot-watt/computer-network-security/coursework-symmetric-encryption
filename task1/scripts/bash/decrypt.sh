#!/bin/bash

keymap='../../keymap-task1-38.txt'
ciphertext='../../cipher-task1-38.txt'

#-------------- Decryption function ---------------

function decrypt() {
    lines=$1
    echo -e "\nAlphabet:|${lines[0]}\nMapping:|${lines[1]}" | column -s"|" -t -e

    plain="$(tr ${lines[1]} ${lines[0]} < $ciphertext)"
    echo -e "\nDecrypted ciphertext:\n\n$plain"
    echo -e "$plain" > './plaintext.txt'
}

#------- Error checking before decryption ---------

if [[ -f "$keymap" ]]; then
    IFS=$'\r\n' GLOBIGNORE='*' command eval "lines=($(cat $keymap))"

    if [[ ${#lines[@]} -eq 2 ]]; then
        decrypt "$lines"
    else 
        echo -e "Error: the keymap needs to contain 2 lines\
                \n\t1- plaintext alphabet (UPPERCASE)\
                \n\t2- ciphertext alphabet (lowercase)"
        exit 1
    fi
else
    echo "Error: keymap file doesn't exists."
    exit 1
fi
